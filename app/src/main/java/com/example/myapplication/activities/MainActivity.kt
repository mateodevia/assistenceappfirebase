package com.example.myapplication.activities

import android.Manifest.permission.CAMERA
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.example.myapplication.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.storage.FirebaseStorage
import java.io.File
import java.io.IOException
import java.nio.file.Files.createFile
import java.text.SimpleDateFormat
import com.google.firebase.storage.StorageReference
import java.net.URI
import java.util.*
import kotlin.Int as Int1


class MainActivity : AppCompatActivity() {
    lateinit var botonTomarFoto: Button;
    val PERMISSION_REQUEST_CODE: Int1 = 101;
    val REQUEST_IMAGE_CAPTURE: Int1 = 1;
    val PICK_IMAGE_REQUEST = 71
    private var mCurrentPhotoPath: String? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        botonTomarFoto = findViewById(R.id.button2)
        botonTomarFoto.setOnClickListener(View.OnClickListener {
            if (verificarPermisos()) tomarFoto() else pedirPermisos()
        })

        botonTomarFoto = findViewById(R.id.button3)
        botonTomarFoto.setOnClickListener(View.OnClickListener {
             launchGallery()
        })
    }

    fun leerPrueba(view: View) {
        val db = FirebaseFirestore.getInstance();
        val settings = FirebaseFirestoreSettings.Builder()
            .setTimestampsInSnapshotsEnabled(true)
            .build()
        db.setFirestoreSettings(settings)

        val col = db.collection("colleccionPrueba").get();

        col.addOnCompleteListener {

            if (it.isSuccessful) {

                for (document in it.result!!) {
                    println(document.data)
                }
            }
            else {
                print("Fallo")
            }
        }
    }

    fun asistir(view: View) {

        val db = FirebaseFirestore.getInstance();
        val settings = FirebaseFirestoreSettings.Builder()
            .setTimestampsInSnapshotsEnabled(true)
            .build()
        db.setFirestoreSettings(settings)

        val col = db.collection("asistencia");

        val result = col.whereEqualTo("codigo", "201630956").get();


        result.addOnSuccessListener { result ->

            val document = result.first()
            val data = document.data
            val nuevoNum = data["asistencias"] as Long + 1L
            data["asistencias"] = nuevoNum
            println("DATA")
            println( data )
            col.document(document.id).update(data)

        }
    }

    private fun verificarPermisos(): Boolean {
        return (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
            android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
    }

    private fun pedirPermisos() {
        ActivityCompat.requestPermissions(this, arrayOf(READ_EXTERNAL_STORAGE, CAMERA), PERMISSION_REQUEST_CODE)
    }

    private fun tomarFoto() {

        val intent: Intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file: File = createFile()

        val uri: Uri = FileProvider.getUriForFile(
            this,
            "com.example.android.fileprovider",
            file
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT,uri)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)

    }

    @Throws(IOException::class)
    private fun createFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = absolutePath
        }
    }

    private fun launchGallery() {

        val intent = Intent()

        intent.type = "image/*"

        intent.action = Intent.ACTION_GET_CONTENT

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST)

    }

    override fun onRequestPermissionsResult(requestCode: Int1, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {

                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    &&grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    tomarFoto()

                } else {
                    Toast.makeText(this, "No me dieron Permiso", Toast.LENGTH_SHORT).show()
                }
                return
            }
            else -> {

            }
        }
    }

    override fun onActivityResult(requestCode: Int1, resultCode: Int1, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {


            //To get the File for further usage
            val auxFile = File(this!!.mCurrentPhotoPath!!)
            println(mCurrentPhotoPath)
            val storage = FirebaseStorage.getInstance()
            val storageReference = storage.getReference();
            val referencia = storageReference.child(mCurrentPhotoPath!!)
            val uploadTask = referencia.putFile(Uri.fromFile(auxFile))

            uploadTask.addOnSuccessListener {

                println("Lo logro")
            }
        }
        else if(requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {

            if(data == null || data.data == null){

                return

            }

            mCurrentPhotoPath = data.dataString
            println("ACAAAAAA")
            println(mCurrentPhotoPath)
            val auxFile = File(this!!.mCurrentPhotoPath!!)
            val storage = FirebaseStorage.getInstance()
            val storageReference = storage.getReference();
            val referencia = storageReference?.child("uploads/" + UUID.randomUUID().toString())
            val uploadTask = referencia.putFile(data.data!!)

            uploadTask.addOnSuccessListener {

                println("Lo logro")
            }

        }
    }
}
